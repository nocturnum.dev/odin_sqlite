package sqlite

import "core:c"
import "core:strings"
import "core:fmt"

open_database :: proc(filename: cstring, flags: OpenFlags) -> (db: DatabaseHandle, err: Result) {
    err = open_v2(filename, &db, c.int(flags), nil)
    return
}

close_database :: proc(db: DatabaseHandle) -> Result {
    return close(db)
}

begin_transaction :: proc(db: DatabaseHandle, errmsg: ^cstring) -> Result { 
    return execute_query(db, "BEGIN TRANSACTION", errmsg)
}

commit_transaction :: proc(db: DatabaseHandle, errmsg: ^cstring) -> Result { 
    return execute_query(db, "COMMIT", errmsg)
}

rollback_transaction :: proc(db: DatabaseHandle, errmsg: ^cstring) -> Result { 
    return execute_query(db, "ROLLBACK", errmsg)
}

execute_query :: proc(db: DatabaseHandle, sql: cstring, errmsg: ^cstring) -> Result {
    return exec(db, sql, default_callback, nil, errmsg)
}

column_bool :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> bool {
    return column_int(pStmt, iCol) == 1 ? true : false,
}

column_text_to_string :: proc(pStmt: sqlite3_stmt, iCol: c.int, allocator := context.allocator) -> string {
    result, err := strings.clone_from_cstring(column_text(pStmt, iCol), allocator)

    if err != nil {
        fmt.printf("Failed to get string from column! Allocator error: {}", err)
    }

    return result
}

bind_string :: proc(pStmt: sqlite3_stmt, iCol: c.int, value: string, allocator := context.temp_allocator) -> Result {
    result, err := strings.clone_to_cstring(value, allocator)
    if err != nil {
        fmt.printf("Failed to get string from column! Allocator error: {}", err)
    }
    return bind_text(pStmt, iCol, result, c.int(len(result)), nil)
}