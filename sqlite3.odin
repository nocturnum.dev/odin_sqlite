package sqlite

when ODIN_OS == .Windows {
foreign import sqlite "./lib/sqlite3.lib"
}  else {
    foreign import sqlite "./lib/sqlite3.o"
}

import "core:c"
import "core:runtime"
import "core:fmt"


SQLITE_VERSION        :: "3.45.1"
SQLITE_VERSION_NUMBER :: 3045001
SQLITE_SOURCE_ID      :: "2024-01-30 16:01:20 e876e51a0ed5c5b3126f52e532044363a014bc594cfefa87ffb5b82257cc467a"

sqlite3       :: distinct rawptr
sqlite3_stmt  :: distinct rawptr
sqlite3_value :: distinct rawptr

DatabaseHandle :: sqlite3
Statement :: sqlite3_stmt

Callback :: #type proc "c" (
    unused: rawptr,
    argc: i32,
    argv: [^]cstring,
    azColName: [^]cstring,
) -> Result

default_callback :: proc "c" (
    unused: rawptr,
    argc: i32,
    argv: [^]cstring,
    azColName: [^]cstring,
) -> Result {
    context = runtime.default_context()
    for i in 0 ..< int(argc) {
        fmt.printf("%v = %v\n", azColName[i], argv[i] != nil ? argv[i] : "-NIL-")
    }
    return .OK
}

Result :: enum c.int {
    OK         = 0,   /* Successful result */
    ERROR      = 1,   /* Generic error */
    INTERNAL   = 2,   /* Internal logic error in SQLite */
    PERM       = 3,   /* Access permission denied */
    ABORT      = 4,   /* Callback routine requested an abort */
    BUSY       = 5,   /* The database file is locked */
    LOCKED     = 6,   /* A table in the database is locked */
    NOMEM      = 7,   /* A malloc() failed */
    READONLY   = 8,   /* Attempt to write a readonly database */
    INTERRUPT  = 9,   /* Operation terminated by sqlite3_interrupt()*/
    IOERR      = 10,  /* Some kind of disk I/O error occurred */
    CORRUPT    = 11,  /* The database disk image is malformed */
    NOTFOUND   = 12,  /* Unknown opcode in sqlite3_file_control() */
    FULL       = 13,  /* Insertion failed because database is full */
    CANTOPEN   = 14,  /* Unable to open the database file */
    PROTOCOL   = 15,  /* Database lock protocol error */
    EMPTY      = 16,  /* Internal use only */
    SCHEMA     = 17,  /* The database schema changed */
    TOOBIG     = 18,  /* String or BLOB exceeds size limit */
    CONSTRAINT = 19,  /* Abort due to constraint violation */
    MISMATCH   = 20,  /* Data type mismatch */
    MISUSE     = 21,  /* Library used incorrectly */
    NOLFS      = 22,  /* Uses OS features not supported on host */
    AUTH       = 23,  /* Authorization denied */
    FORMAT     = 24,  /* Not used */
    RANGE      = 25,  /* 2nd parameter to sqlite3_bind out of range */
    NOTADB     = 26,  /* File opened that is not a database file */
    NOTICE     = 27,  /* Notifications from sqlite3_log() */
    WARNING    = 28,  /* Warnings from sqlite3_log() */
    ROW        = 100, /* sqlite3_step() has another row ready */
    DONE       = 101, /* sqlite3_step() has finished executing */
}

OpenFlags :: enum c.int {
    READONLY      = 0x00000001, /* Ok for sqlite3_open_v2() */
    READWRITE     = 0x00000002, /* Ok for sqlite3_open_v2() */
    CREATE        = 0x00000004, /* Ok for sqlite3_open_v2() */
    DELETEONCLOSE = 0x00000008, /* VFS only */
    EXCLUSIVE     = 0x00000010, /* VFS only */
    AUTOPROXY     = 0x00000020, /* VFS only */
    URI           = 0x00000040, /* Ok for sqlite3_open_v2() */
    MEMORY        = 0x00000080, /* Ok for sqlite3_open_v2() */
    MAIN_DB       = 0x00000100, /* VFS only */
    TEMP_DB       = 0x00000200, /* VFS only */
    TRANSIENT_DB  = 0x00000400, /* VFS only */
    MAIN_JOURNAL  = 0x00000800, /* VFS only */
    TEMP_JOURNAL  = 0x00001000, /* VFS only */
    SUBJOURNAL    = 0x00002000, /* VFS only */
    SUPER_JOURNAL = 0x00004000, /* VFS only */
    NOMUTEX       = 0x00008000, /* Ok for sqlite3_open_v2() */
    FULLMUTEX     = 0x00010000, /* Ok for sqlite3_open_v2() */
    SHAREDCACHE   = 0x00020000, /* Ok for sqlite3_open_v2() */
    PRIVATECACHE  = 0x00040000, /* Ok for sqlite3_open_v2() */
    WAL           = 0x00080000, /* VFS only */
    NOFOLLOW      = 0x01000000, /* Ok for sqlite3_open_v2() */
    EXRESCODE     = 0x02000000, /* Extended result codes */
}

@(default_calling_convention="c", link_prefix="sqlite3_")
foreign sqlite {
    open            :: proc(filename: cstring, ppDb: ^DatabaseHandle) -> Result ---
    open_16         :: proc(filename: cstring, ppDb: ^DatabaseHandle) -> Result ---
	open_v2         :: proc(filename: cstring, ppDb: ^DatabaseHandle, flags: c.int, zVfs: cstring) -> Result ---
    close           :: proc(db: DatabaseHandle) -> Result  ---

    exec            :: proc(db: DatabaseHandle, sql: cstring, callback: Callback, arg: rawptr, errmsg: ^cstring) -> Result ---
    
    @(link_name = "sqlite3_prepare_v2")
    prepare      :: proc(db: DatabaseHandle, sql: cstring, nByte: c.int, ppStmt: ^sqlite3_stmt, pzTail: ^cstring) -> Result ---
    
    step            :: proc(pStmt: sqlite3_stmt) -> Result ---
    reset           :: proc(pStmt: sqlite3_stmt) -> Result ---
    finalize        :: proc(pStmt: sqlite3_stmt) -> Result ---
    clear_bindings  :: proc(pStmt: sqlite3_stmt) -> Result ---

    column_count    :: proc(pStmt: sqlite3_stmt) -> Result ---
    column_name     :: proc(pStmt: sqlite3_stmt, N: c.int) -> cstring ---
    column_type     :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> DataType ---
    column_int      :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> c.int ---
    column_text     :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> cstring ---
    column_blob     :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> rawptr ---
    column_double   :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> c.double ---
    column_bytes    :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> Result ---
    column_bytes16  :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> Result ---
    column_value    :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> sqlite3_value ---
    column_int64    :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> c.longlong ---
    column_text16   :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> rawptr ---
    column_text16le :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> rawptr ---
    column_text16be :: proc(pStmt: sqlite3_stmt, iCol: c.int) -> rawptr ---

    bind_blob       :: proc(pStmt: sqlite3_stmt, idx: c.int, value: rawptr, n: c.int, destructor: rawptr) -> Result ---
    bind_double     :: proc(pStmt: sqlite3_stmt, idx: c.int, value: c.double) -> Result ---
    bind_int        :: proc(pStmt: sqlite3_stmt, i_col: i32, value: i32) -> Result ---
    bind_int64      :: proc(pStmt: sqlite3_stmt, idx: c.int, value: c.longlong) -> Result ---
    bind_null       :: proc(pStmt: sqlite3_stmt, idx: c.int) -> Result ---
    bind_value      :: proc(pStmt: sqlite3_stmt, idx: c.int, value: sqlite3_value) -> Result ---
    bind_blob64     :: proc(pStmt: sqlite3_stmt, idx: c.int, value: rawptr, n: c.ulonglong, destructor: rawptr) -> Result ---
    bind_text       :: proc(pStmt: sqlite3_stmt, i_col: c.int, text: cstring, n_bytes: c.int, dealloc: proc(p: rawptr)) -> Result ---
    bind_text16     :: proc(pStmt: sqlite3_stmt, idx: c.int, value: rawptr, n: c.int, destructor: rawptr) -> Result ---
    bind_text64     :: proc(pStmt: sqlite3_stmt, idx: c.int, value: cstring, n: c.ulonglong, destructor: rawptr, encoding: TextEncoding) -> Result ---
    bind_pointer    :: proc(pStmt: sqlite3_stmt, idx: c.int, value: rawptr, type: cstring, destructor: rawptr) -> Result ---
    bind_zeroblob   :: proc(pStmt: sqlite3_stmt, idx: c.int, n: c.int) -> Result ---
    bind_zeroblob64 :: proc(pStmt: sqlite3_stmt, idx: c.int, n: c.ulonglong) -> Result ---

    last_insert_rowid :: proc(db: DatabaseHandle) -> c.ulonglong ---
   
    errcode         :: proc(db: DatabaseHandle) -> Result ---
    errmsg          :: proc(db: DatabaseHandle) -> cstring ---
    changes         :: proc(pDb: DatabaseHandle) -> Result ---
    free            :: proc(p: rawptr) ---

    libversion        :: proc() -> cstring ---
    sourceid          :: proc() -> cstring ---
    libversion_number :: proc() -> c.int ---
}

DataType :: enum c.int {
    Invalid = 0,
    Integer = 1,
    Float   = 2,
    Text    = 3,
    Blob    = 4,
    Null    = 5,
}

TextEncoding :: enum u8 {
    UTF8          = 1,
    UTF16LE       = 2,
    UTF16BE       = 3,
    UTF16         = 4,
    ANY           = 5,
    UTF16_Aligned = 8,
}