package sqlite3_test

import sqlite3 "../../odin_sqlite"
import "core:fmt"

main :: proc() {
	fmt.println("Opening Database")

	db, open_result := sqlite3.open_database("./test.db", .READWRITE | .CREATE)
	if open_result != .OK {
		fmt.printf("failed to open db with error {}", open_result)
	}

	fmt.println("Executing Test query")
	
	query_result := sqlite3.execute_query(db, "CREATE TABLE IF NOT EXISTS test (id INTEGER PRIMARY KEY, value TEXT);")

	if query_result != .OK {
		fmt.printf("failed to create table with error {}", query_result)
	}

	fmt.println("Closing DB")
	sqlite3.close(db)
}