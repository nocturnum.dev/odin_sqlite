#!/bin/bash

# Compile SQLite3 source file into object file
clang -c -DSQLITE_THREADSAFE=0 sqlite3.c

# Create the static library
ar rcs sqlite3.a sqlite3.o